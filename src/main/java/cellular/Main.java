package cellular;

import cellular.cellstate.ICellState;
import cellular.gui.CellAutomataGUI;
import datastructure.Grid;
import datastructure.IGrid;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {

		//ICellAutomaton ca = new LangtonsAnt(50, 50, "RRLLLRLLLRRR");
		//ICellAutomaton ca = new SeedsAutomaton(50, 50);
		 ICellAutomaton ca = new GameOfLife(50, 50);

		CellAutomataGUI.run(ca);

	}

}
